#include <linux/kernel.h>
#include <linux/syscalls.h>

static void print_pgd_vaddr(void)
{
	printk(KERN_INFO "The physical address of the Page Directory of current"
			" process is 0x%08lX,\n"
			"\t\tand the current value in the CR3 is 0x%08lX.\n"
			"\t\tThey should be the same.\n",
			(unsigned long)virt_to_phys((unsigned long *)current->mm->pgd),
			(unsigned long)read_cr3());
}

static void print_pgd_range(unsigned short start, unsigned short end)
{
	unsigned long addr;
	unsigned short i, j;
	pgd_t *base = current->mm->pgd;
	pgd_t *pgd;
	pmd_t *pmd;
	pte_t *pte;

	for (i = start; i < end; i++) {
		addr = i << PGDIR_SHIFT;
		pgd = base + i;
		pmd = pmd_offset(pud_offset(pgd, addr), addr);

		if (!pmd_present(*pmd)) {
			printk(KERN_INFO "[%04u] NULL\n", i);
			continue;
		}

		if (pmd_large(*pmd)) {
			printk(KERN_INFO "[%04u] 4M 0x%08lX\n", i, pmd_val(*pmd));
			continue;
		}

		printk(KERN_INFO "[%04u] Page Table 0x%08lX\n", i, pmd_val(*pmd));

		for (j = 0; j < PTRS_PER_PTE; j++) {
			pte = pte_offset_kernel(pmd, addr | (j << PAGE_SHIFT));
			if (!pte_present(*pte))
				continue;
			printk(KERN_INFO "\t\t[%04u] 0x%08lX\n", j, pte_val(*pte));
		}
	}
}

SYSCALL_DEFINE0(csie_ncu_survey_TT)
{
	printk(KERN_INFO "###################################\n");
	printk(KERN_INFO "# Entering 'csie_ncu_survey_TT()' #\n");
	printk(KERN_INFO "###################################\n");
	printk(KERN_INFO "\n");

	printk(KERN_INFO "#####\n");
	printk(KERN_INFO "# 1 #\n");
	printk(KERN_INFO "#####\n");
	print_pgd_vaddr();
	printk(KERN_INFO "\n");

	printk(KERN_INFO "#####\n");
	printk(KERN_INFO "# 2 #\n");
	printk(KERN_INFO "#####\n");
	print_pgd_range(992, 1024);
	printk(KERN_INFO "\n");

	printk(KERN_INFO "##################################\n");
	printk(KERN_INFO "# Leaving 'csie_ncu_survey_TT()' #\n");
	printk(KERN_INFO "##################################\n");
	return 0;
}
